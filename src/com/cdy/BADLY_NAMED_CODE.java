package com.cdy;

/**
 * 在项目根路径下 执行以下代码 , 测试编译时的插件注解处理器
 * javac -cp out/production/javac  -processor com.cdy.NameCheckProcessor  src/com/cdy/BADLY_NA
 * MED_CODE.java
 */
public class BADLY_NAMED_CODE {

	enum colors {
		red, blue, green;
	}

	static final int _FORTY_TWO = 42;

	public static int NOT_A_CONSTANT = _FORTY_TWO;

	protected void BADLY_NAMED_CODE() {
		return;
	}

	public void NOTcamelCASEmethodNAME() {
		return;
	}
}


